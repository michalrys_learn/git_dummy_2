# Purpose
This is test repository for proof of concept: how to do an automatic update for several repositories (more than 50) based on one repository treated as a template repo.

# How to check it
1. This is a repo which shall be updated in an automatic way (using some script) based on template repo.
2. Go to template repo: https://gitlab.com/michalrys_learn/git_dummy_1
3. Check a README.md file there. Test bash script which is written there.
